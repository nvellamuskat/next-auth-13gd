import User from "../../models/user";
import dbConnect from "../../config/dbConnect";

export default async function handler(req, res) {
  if (req.method === "POST") {
    try {
      await dbConnect();

      const { name, email, password } = req.body;

      const user = await User.create({ name, email, password });

      res.status(201).json({ user });
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: 'An error occurred while trying to create a user' });
    }
  }
}
